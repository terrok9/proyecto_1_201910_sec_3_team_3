package contenedora;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class VOMovingViolations {

private int ObjectId;

private String Location;

private String Date;

private int valor;

private String accidente;

private String violation;

private String violationCode;

private int AddressId;

private int StreetSegId;

private double FineAmount;

private String penalty1;

private String penalty2;
public VOMovingViolations(int pObjectId, String pLocation, String pDate, int pValor, String pAccidente, String pViolation, String pViolationCode, int pAddresId, int pStreetId, double pFineAmount, String pPenalty1, String pPenalty2){
  ObjectId = pObjectId;
  Location = pLocation;
  Date = pDate;
  valor = pValor;
  accidente =  pAccidente;
  violation = pViolation;
  violationCode = pViolationCode;
  AddressId = pAddresId;
  StreetSegId = pStreetId;
  FineAmount = pFineAmount;
  penalty1 = pPenalty1;
  penalty2 = pPenalty2;
}

/**
 * @return id - Identificador único de la infracción
 */
public int objectId() {
 // TODO Auto-generated method stub
 return ObjectId;
} 


/**
 * @return location - Dirección en formato de texto.
 */
public String getLocation() {
 // TODO Auto-generated method stub
 return Location;
}

/**
 * @return date - Fecha cuando se puso la infracción .
 */
public String getTicketIssueDate() {
 // TODO Auto-generated method stub
 return Date;
}

/**
 * @return totalPaid - Cuanto dinero efectivamente pagó el que recibió la infracción en USD.
 */
public int getTotalPaid() {
 // TODO Auto-generated method stub
 return valor;
}

/**
 * @return accidentIndicator - Si hubo un accidente o no.
 */
public String  getAccidentIndicator() {
 // TODO Auto-generated method stub
 return accidente;
}

/**
 * @return description - Descripción textual de la infracción.
 */
public String  getViolationDescription() {
 // TODO Auto-generated method stub
 return violation;
}

public int getAddressId() {
	// TODO Auto-generated method stub
	return AddressId;
}

public int getStreetSegId() {
	// TODO Auto-generated method stub
	return StreetSegId;
}
public String getPenalty1(){
	return penalty1;
}
public String getPenalty2(){
	return penalty2;
}
public String getViolationCode(){
	return violationCode;
}
public double getFineAmount(){
	return FineAmount;
}
public double getAvgFineAmt(){
	double retorno = 0;
	ArrayList<Double> promedio = new ArrayList<Double>();
	promedio.add(FineAmount);
	for (int i = 0; i < promedio.size(); i++){
		retorno += promedio.get(i);
	}
	retorno = retorno/promedio.size();
	return retorno;
}
}
