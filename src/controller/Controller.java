package controller;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import java.io.File;

import com.opencsv.CSVReader;
import model.util.*;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.Lista;
import model.data_structures.Stack;
import consola.*;
import contenedora.*;

public class Controller {
    private String[] cuatrimestre;
    
	private MovingViolationsManagerView view;
	
	/**
	 * Cola donde se van a cargar los datos de los archivos
	 */
	private IQueue<VOMovingViolations> movingViolationsQueue;
	
	/**
	 * Pila donde se van a cargar los datos de los archivos
	 */
	private IStack<VOMovingViolations> movingViolationsStack;
	/**
	 * Muestras de datos
	 */
	// Muestra obtenida de los datos cargados 
		private Comparable<VOMovingViolations> [ ] muestra;

		// Copia de la muestra de datos a ordenar 
		private Comparable<VOMovingViolations> [ ] muestraCopia;
	public Controller() {
		view = new MovingViolationsManagerView();
		
		//TODO, inicializar la pila y la cola
		
		movingViolationsQueue = new Lista<VOMovingViolations>();
		movingViolationsStack = new Stack<VOMovingViolations>(0);
		cuatrimestre = new String[4];
	}
	/**
	 * Generar una muestra aleatoria de tamaNo n de los datos leidos.
	 * Los datos de la muestra se obtienen de las infracciones guardadas en la Estructura de Datos.
	 * @param n tamaNo de la muestra, n > 0
	 * @return muestra generada
	 */
	public Comparable<VOMovingViolations> [ ] generarMuestra( int n )
	{
		long startTime = System.currentTimeMillis();
		muestra = new Comparable[ n ];
		
		// TODO Llenar la muestra aleatoria con los datos guardados en la estructura de datos
		long endTime = System.currentTimeMillis();
		long timeTaken = endTime - startTime;
		System.out.println("QuickSort: " + timeTaken);
		return muestra;
		
	}
	
	/**
	 * Generar una copia de una muestra. Se genera un nuevo arreglo con los mismos elementos.
	 * @param muestra - datos de la muestra original
	 * @return copia de la muestra
	 */
	public Comparable<VOMovingViolations> [ ] obtenerCopia( Comparable<VOMovingViolations> [ ] muestra)
	{
		long startTime = System.currentTimeMillis();
		Comparable<VOMovingViolations> [ ] copia = new Comparable[ muestra.length ]; 
		for ( int i = 0; i < muestra.length; i++)
		{    copia[i] = muestra[i];    }
		long endTime = System.currentTimeMillis();
		long timeTaken = endTime - startTime;
		System.out.println("QuickSort: " + timeTaken);
		return copia;
	}
	
	/**
	 * Ordenar datos aplicando el algoritmo ShellSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	public void ordenarShellSort( Comparable<VOMovingViolations>[ ] datos ) {
		long startTime = System.currentTimeMillis();
		Sort.ordenarShellSort(datos);
		long endTime = System.currentTimeMillis();
		long timeTaken = endTime - startTime;
		System.out.println("QuickSort: " + timeTaken);
	}
	
	/**
	 * Ordenar datos aplicando el algoritmo MergeSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	public void ordenarMergeSort( Comparable<VOMovingViolations>[ ] datos ) {
		long startTime = System.currentTimeMillis();
		Sort.ordenarMergeSort(datos);
		long endTime = System.currentTimeMillis();
		long timeTaken = endTime - startTime;
		System.out.println("QuickSort: " + timeTaken);
	}

	/**
	 * Ordenar datos aplicando el algoritmo QuickSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	public void ordenarQuickSort( Comparable<VOMovingViolations>[ ] datos ) {
		long startTime = System.currentTimeMillis();
		Sort.ordenarQuickSort(datos);
		long endTime = System.currentTimeMillis();
		long timeTaken = endTime - startTime;
		System.out.println("QuickSort: " + timeTaken);
	}

	/**
	 * Invertir una muestra de datos (in place).
	 * datos[0] y datos[N-1] se intercambian, datos[1] y datos[N-2] se intercambian, datos[2] y datos[N-3] se intercambian, ...
	 * @param datos - conjunto de datos a invertir (inicio) y conjunto de datos invertidos (final)
	 */
	public void invertirMuestra( Comparable[ ] datos ) {
		// TODO implementar
		long startTime = System.currentTimeMillis();
		for (int i = 0; i < datos.length; i++){
			 Comparable datostemp = datos[i];
			 datos[ i] = datos[datos.length - i];
             datos[ datos.length] = datostemp;
		}
		long endTime = System.currentTimeMillis();
		long timeTaken = endTime - startTime;
		System.out.println("QuickSort: " + timeTaken);
	}
	public void run() {
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		Controller controller = new Controller();
		
		while(!fin)
		{
			view.printMenu();
			
			int option = sc.nextInt();
			
			switch(option)
			{
				case 0:
					view.printMessage("Ingrese el cuatrimestre (1, 2 o 3)");
					int numeroCuatrimestre = sc.nextInt();
					controller.loadMovingViolations(numeroCuatrimestre);
					break;
				
				case 1:
					IQueue<VOMovingViolations> isUnique = controller.verifyObjectIDIsUnique();
					view.printMessage("El objectId es �nico: " + isUnique);
					break;
					
				case 2:
					
					view.printMessage("Ingrese la fecha con hora inicial (Ej : 28/03/2017T15:30:20)");
					LocalDateTime fechaInicialReq2A = convertirFecha_Hora_LDT(sc.next());
					
					view.printMessage("Ingrese la fecha con hora final (Ej : 28/03/2017T15:30:20)");
					LocalDateTime fechaFinalReq2A = convertirFecha_Hora_LDT(sc.next());
					
					IQueue<VOMovingViolations> resultados2 = controller.getMovingViolationsInRange(fechaInicialReq2A, fechaFinalReq2A);
					
					view.printMovingViolationsReq2(resultados2);
					
					break;
					
				case 3:
					
					view.printMessage("Ingrese el VIOLATIONCODE (Ej : T210)");
					String violationCode3 = sc.next();
					
					double [] promedios3 = controller.avgFineAmountByViolationCode(violationCode3);
					
					view.printMessage("FINEAMT promedio sin accidente: " + promedios3[0] + ", con accidente:" + promedios3[1]);
					break;
						
					
				case 4:
					
					view.printMessage("Ingrese el ADDRESS_ID");
					String addressId4 = sc.next();

					view.printMessage("Ingrese la fecha con hora inicial (Ej : 28/03/2017)");
					LocalDate fechaInicialReq4A = convertirFecha(sc.next());
					
					view.printMessage("Ingrese la fecha con hora final (Ej : 28/03/2017)");
					LocalDate fechaFinalReq4A = convertirFecha(sc.next());
					
					IStack<VOMovingViolations> resultados4 = controller.getMovingViolationsAtAddressInRange(addressId4, fechaInicialReq4A, fechaFinalReq4A);
					
					view.printMovingViolationsReq4(resultados4);
					
					break;
					
				case 5:
					view.printMessage("Ingrese el limite inferior de FINEAMT  (Ej: 50)");
					double limiteInf5 = sc.nextDouble();
					
					view.printMessage("Ingrese el limite superior de FINEAMT  (Ej: 50)");
					double limiteSup5 = sc.nextDouble();
					
					IQueue<VOMovingViolations> violationCodes = controller.violationCodesByFineAmt(limiteInf5, limiteSup5);
					view.printViolationCodesReq5(violationCodes);
					break;
				
				case 6:
					
					view.printMessage("Ingrese el limite inferior de TOTALPAID (Ej: 200)");
					double limiteInf6 = sc.nextDouble();
					
					view.printMessage("Ingrese el limite superior de TOTALPAID (Ej: 200)");
					double limiteSup6 = sc.nextDouble();
					
					view.printMessage("Ordenar Ascendentmente: (Ej: true)");
					boolean ascendente6 = sc.nextBoolean();				
					
					IStack<VOMovingViolations> resultados6 = controller.getMovingViolationsByTotalPaid(limiteInf6, limiteSup6, ascendente6);
					view.printMovingViolationReq6(resultados6);
					break;
					
				case 7:
					
					view.printMessage("Ingrese la hora inicial (Ej: 23)");
					int horaInicial7 = sc.nextInt();
					
					view.printMessage("Ingrese la hora final (Ej: 23)");
					int horaFinal7 = sc.nextInt();
					
					IQueue<VOMovingViolations> resultados7 = controller.getMovingViolationsByHour(horaInicial7, horaFinal7);
					view.printMovingViolationsReq7(resultados7);
					break;
					
				case 8:
					
					view.printMessage("Ingrese el VIOLATIONCODE (Ej : T210)");
					String violationCode8 = sc.next();
					
					double [] resultado8 = controller.avgAndStdDevFineAmtOfMovingViolation(violationCode8);
					
					view.printMessage("FINEAMT promedio: " + resultado8[0] + ", desviaci�n estandar:" + resultado8[1]);
					break;
					
				case 9:
					
					view.printMessage("Ingrese la hora inicial (Ej: 23)");
					int horaInicial9 = sc.nextInt();
					
					view.printMessage("Ingrese la hora final (Ej: 23)");
					int horaFinal9 = sc.nextInt();
					
					int resultado9 = controller.countMovingViolationsInHourRange(horaInicial9, horaFinal9);
					
					view.printMessage("N�mero de infracciones: " + resultado9);
					break;
				
				case 10:
					view.printMovingViolationsByHourReq10();
					break;
					
				case 11:
					view.printMessage("Ingrese la fecha inicial (Ej : 28/03/2017)");
					LocalDate fechaInicial11 = convertirFecha(sc.next());
					
					view.printMessage("Ingrese la fecha final (Ej : 28/03/2017)");
					LocalDate fechaFinal11 = convertirFecha(sc.next());
					
					double resultados11 = controller.totalDebt(fechaInicial11, fechaFinal11);
					view.printMessage("Deuda total "+ resultados11);
					break;
				
				case 12:	
					view.printTotalDebtbyMonthReq12();
					
					break;
					
				case 13:	
					fin=true;
					sc.close();
					break;
			}
		}

}
    
	public String[] meses(int pNumeroDeCuatrimestre){
		if(pNumeroDeCuatrimestre == 1){
			cuatrimestre[0] = "January";
			cuatrimestre[1] = "February";
			cuatrimestre[2] = "March";
			cuatrimestre[3] = "April";
		}
		else if(pNumeroDeCuatrimestre == 2){
        	cuatrimestre[0] = "May";
        	cuatrimestre[1] = "June";
        	cuatrimestre[2] = "July";
        	cuatrimestre[3] = "August";
		}
        else{
        	cuatrimestre[0] = "September";
        	cuatrimestre[1] = "October";
        	cuatrimestre[2] = "November";
        	cuatrimestre[3] = "December";
        }
		return cuatrimestre;
	}

	public void loadMovingViolations(int pNumeroDeCuatrimestre) 
	{
		// TODO
		cuatrimestre = meses(pNumeroDeCuatrimestre);
		try
		{
			 CSVReader reader = new CSVReader(new FileReader("data/Moving_Violations_Issued_in_" + cuatrimestre[0]+ "_2018.csv" ));
			 CSVReader reader2 = new CSVReader(new FileReader("data/Moving_Violations_Issued_in_" + cuatrimestre[1]+ "_2018.csv"));
			 CSVReader reader3 = new CSVReader(new FileReader("data/Moving_Violations_Issued_in_" + cuatrimestre[2]+ "_2018.csv"));
			 CSVReader reader4 = new CSVReader(new FileReader("data/Moving_Violations_Issued_in_" + cuatrimestre[3]+ "_2018.csv"));
      String[] nextLine;
      reader.readNext();
      while((nextLine = reader.readNext()) != null)
      {
    	
    	VOMovingViolations a = new VOMovingViolations(Integer.parseInt(nextLine[0]), nextLine[1], nextLine[12], Integer.parseInt(nextLine[8]), nextLine[11], nextLine[14], nextLine[13], Integer.parseInt(nextLine[2]), Integer.parseInt(nextLine[3]), Double.parseDouble(nextLine[7]), nextLine[9], nextLine[10]);
    	System.out.println("prueba de que se cargan los datos, se le imprime el objectID con el total pagado");
    	System.out.println(a.objectId() +" "+a.getTotalPaid());
    	movingViolationsQueue.enqueue(a);
    	movingViolationsStack.push(a);
    	
    	
      }
      String[] nextLine2;
      reader2.readNext();
      while((nextLine2 = reader2.readNext()) != null)
      {
    	VOMovingViolations a = new VOMovingViolations(Integer.parseInt(nextLine2[0]), nextLine2[1], nextLine2[12], Integer.parseInt(nextLine2[8]), nextLine2[11], nextLine2[14], nextLine2[13], Integer.parseInt(nextLine2[2]), Integer.parseInt(nextLine2[3] ), Double.parseDouble(nextLine2[7]), nextLine2[9], nextLine2[10]);
    	System.out.println("prueba de que se cargan los datos, se le imprime el objectID con el total pagado");
    	System.out.println(a.objectId() +" "+a.getTotalPaid());
    	movingViolationsQueue.enqueue(a);
    	movingViolationsStack.push(a);
    	
    	
      }
      String[] nextLine3;
      reader3.readNext();
      while((nextLine3 = reader3.readNext()) != null)
      {
    	
    	VOMovingViolations a = new VOMovingViolations(Integer.parseInt(nextLine3[0]), nextLine3[1], nextLine3[12], Integer.parseInt(nextLine3[8]), nextLine3[11], nextLine3[14], nextLine3[13], Integer.parseInt(nextLine3[2]), Integer.parseInt(nextLine3[3]), Double.parseDouble(nextLine3[7]), nextLine3[9], nextLine3[10] );
    	System.out.println("prueba de que se cargan los datos, se le imprime el objectID con el total pagado");
    	System.out.println(a.objectId() +" "+a.getTotalPaid());
    	movingViolationsQueue.enqueue(a);
    	movingViolationsStack.push(a);
    	
      }
      String[] nextLine4;
      reader4.readNext();
      while((nextLine4 = reader4.readNext()) != null)
      {
    	
    	VOMovingViolations a = new VOMovingViolations(Integer.parseInt(nextLine4[0]), nextLine4[1], nextLine4[12], Integer.parseInt(nextLine4[8]), nextLine4[11], nextLine4[14], nextLine4[13], Integer.parseInt(nextLine4[2]), Integer.parseInt(nextLine4[3] ), Double.parseDouble(nextLine4[7]), nextLine4[9], nextLine4[10]);
    	System.out.println("prueba de que se cargan los datos, se le imprime el objectID con el total pagado");
    	System.out.println(a.objectId() +" "+a.getTotalPaid());
    	movingViolationsQueue.enqueue(a);
    	movingViolationsStack.push(a);
    	
      }
      reader.close();
      reader2.close();
      reader3.close();
      reader4.close();
		}
    catch(Exception e)
    {
    	e.getMessage();
    }
	}
	
	public IQueue <VOMovingViolations> getDailyStatistics ()
	{
		// TODO
		IQueue<VOMovingViolations> movingViolationsQueue2 = null;
		int q = 0;
		for(int i = 0; i<movingViolationsQueue.size(); i++)
		{
			VOMovingViolations x = (VOMovingViolations) movingViolationsQueue; 
			int pValor = x.getTotalPaid();
			for(int j = 0; j<movingViolationsQueue.size(); j++)
			{
				
				VOMovingViolations y = (VOMovingViolations) movingViolationsQueue; 
				int pValo = y.getTotalPaid();
				if(x.getTicketIssueDate().equals(y.getTicketIssueDate()))
				{
					int wq = 0;
					if(x.getAccidentIndicator().equals(y.getAccidentIndicator()))
							{
						wq ++;
							}
					q ++;
					pValor += pValo;
					String qs = ""+q;
					String wqs = ""+wq;
					VOMovingViolations a = new VOMovingViolations(x.objectId(), x.getLocation(), x.getTicketIssueDate(), pValor, qs, wqs, x.getViolationCode(), x.getAddressId(), x.getStreetSegId(), x.getFineAmount(), x.getPenalty1(), x.getPenalty2());
					//Date, valor, accidente, violaci�n
					movingViolationsQueue.enqueue(a);
				}
			}
		}
		return movingViolationsQueue2;
	}
	
	public IStack<VOMovingViolations> nLastAccidents(int n) 
	{
		// TODO
		IStack<VOMovingViolations> retorno = null;
		for(int i = 0; i < movingViolationsStack.size(); i++){
			VOMovingViolations a = (VOMovingViolations) movingViolationsStack;
			if (i <= n){
				int codigo = a.objectId();
				String date = a.getTicketIssueDate();
				String location = a.getLocation();
				String violation = a.getViolationDescription();
				System.out.println(codigo + " " + date + " " + location + " " + violation);
			}
			movingViolationsStack.pop();
			retorno = movingViolationsStack;
		}
		return retorno;
	}
	public IQueue<VOMovingViolations> verifyObjectIDIsUnique() {
		IQueue<VOMovingViolations> retorno = new Lista<VOMovingViolations>();
		long startTime = System.currentTimeMillis();
		boolean centinela = false;
		for (int i = 0; i < movingViolationsQueue.size(); i++){
			VOMovingViolations issue = movingViolationsQueue.dequeue();
			for (int j = i + 1; j < movingViolationsQueue.size(); j++){
				VOMovingViolations issue2 = movingViolationsQueue.dequeue();
				if (issue.objectId() == issue2.objectId()){
					retorno.enqueue(issue2);
					centinela = true;
				}
			}
		}
		if (!centinela){
			System.out.println("Ning�n Id Repetido");
		}
		else{
			System.out.println("Hay Id repetidos" + retorno.toString());
		}
		long endTime = System.currentTimeMillis();
		long timeTaken = endTime - startTime;
		System.out.println("Sort: " + timeTaken);
		return retorno;
	}

	public IQueue<VOMovingViolations> getMovingViolationsInRange(LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		// TODO Auto-generated method stub
		IQueue<VOMovingViolations> retorno = new Lista<VOMovingViolations>();
		for (int i = 0; i < movingViolationsQueue.size(); i++){
		VOMovingViolations object = movingViolationsQueue.dequeue();
		LocalDateTime fecha =  convertirFecha_Hora_LDT(object.getTicketIssueDate());
		if(fechaInicial.isBefore(fecha) && fechaInicial.isAfter(fecha)){
			retorno.enqueue(object);
			System.out.println(object.objectId() + " "+ object.getTicketIssueDate());
		}
		}
		return retorno;
	}

	public double[] avgFineAmountByViolationCode(String violationCode3) {
		double fineno = 0;
		double fineyes = 0;
		for (int i = 0; i < movingViolationsStack.size(); i++){
			VOMovingViolations object = movingViolationsStack.pop();
			if(object.getViolationCode().equals(violationCode3)){
				if(object.getAccidentIndicator().equals("No")){
					 fineno = object.getAvgFineAmt();
				}
				else{
					fineyes = object.getAvgFineAmt();
				}
			}
		}
		return new double [] {fineno, fineyes};
	}

	public IStack<VOMovingViolations> getMovingViolationsAtAddressInRange(String addressId, LocalDate fechaInicial, LocalDate fechaFinal) {
		// TODO Auto-generated method stub
		IStack<VOMovingViolations> retorno = new Stack<VOMovingViolations>(20);
		for(int i = 0; i < movingViolationsStack.size(); i++){
			VOMovingViolations object = movingViolationsStack.pop();
			LocalDateTime fecha =  convertirFecha_Hora_LDT(object.getTicketIssueDate());
			String address = object.getAddressId() + "";
			if(fechaInicial.isBefore(fecha.toLocalDate()) && fechaFinal.isAfter(fecha.toLocalDate()) && address.equals(addressId)){
				System.out.println(object.objectId() + " "+ object.getTicketIssueDate() + " " + object.getStreetSegId() + " " + object.getAddressId());
				retorno.push(object);
			}
		}
		return retorno;
	}

	public IQueue<VOMovingViolations> violationCodesByFineAmt(double limiteInf5, double limiteSup5) {
	    IQueue<VOMovingViolations> cola = null;
	    double contadora = 0;
		double contadora2 = 0;
		double tot = 0;
	    for(int i= 0;i<movingViolationsQueue.size(); i++ )
	    {
	    	VOMovingViolations temp = movingViolationsQueue.dequeue();
	    	for(int j = i+1; j<movingViolationsQueue.size(); j++)
	    	{
	    		VOMovingViolations temp2 = movingViolationsQueue.dequeue();
	    		if(temp.getViolationCode().equals(temp2.getViolationCode()))
	    		{
	    			contadora += temp.getTotalPaid();
					contadora2 ++;
					
	    		}
	    		cola.enqueue(temp);
	    		
	    	}
	    	tot = contadora/contadora2;
	    	
	    	if(tot<limiteSup5 && tot>limiteInf5)
	    	{
	    		System.out.println(temp.getViolationCode() + tot);
	    	}
	    }
	    return cola;
	}

	public IStack<VOMovingViolations> getMovingViolationsByTotalPaid(double limiteInf6, double limiteSup6,
			boolean ascendente6) {
		IStack<VOMovingViolations> cola= new Stack<VOMovingViolations>(10);
		Iterator<VOMovingViolations> iter = movingViolationsStack.iterator();
		while(iter.hasNext())
		{
			VOMovingViolations temp = iter.next();
			if(temp.getTotalPaid()>limiteInf6 && temp.getTotalPaid()<limiteSup6)
			{
				cola.push(temp);
				System.out.println(temp.objectId()+""+ temp.getTicketIssueDate()+ "" + temp.getTotalPaid());
			}
		}
		
		return cola;
	}

	public IQueue<VOMovingViolations> getMovingViolationsByHour(int horaInicial7, int horaFinal7) {
		Iterator<VOMovingViolations> cola1 = movingViolationsQueue.iterator();
		IQueue<VOMovingViolations> cola = new Lista<VOMovingViolations>();
		while(cola1.hasNext())
		{
			VOMovingViolations temp = cola1.next();
			LocalDateTime hora = convertirFecha_Hora_LDT(temp.getTicketIssueDate());
			int dia = hora.getDayOfMonth();
			if(horaInicial7 < dia && horaFinal7 > dia)
			{
				cola.enqueue(temp);
				System.out.println(temp.objectId()+""+ temp.getTicketIssueDate()+ "" + temp.getViolationDescription());
			}
			
		}
		return cola;
	
	}

	public double[] avgAndStdDevFineAmtOfMovingViolation(String violationCode8) {
		
		double contadora = 0;
		double contadora2 = 0;
		double rango = 0;
	    ArrayList x = new ArrayList();
		double tot = 0;
		Iterator<VOMovingViolations> cola = movingViolationsQueue.iterator();
		while(cola.hasNext())
		{
			VOMovingViolations temp = cola.next();
			if(temp.getViolationCode().equals(violationCode8))
			{
				contadora += temp.getTotalPaid();
				contadora2 ++;
				x.add(temp.getTotalPaid());
			}
		}
		tot = contadora/contadora2;
		
		
		return new double [] {contadora, contadora2};
	}

	public int countMovingViolationsInHourRange(int horaInicial9, int horaFinal9){
		// TODO Auto-generated method stub
		LocalDateTime hora1 = LocalDateTime.parse("00:00:00");
		LocalDateTime hora2 = LocalDateTime.parse("01:00:00");
		int contadora = 0;
		for(int i = 0; i <24; i++)
		{
			for(int j = 0; j<movingViolationsQueue.size(); j++)
			{
				VOMovingViolations temp = movingViolationsQueue.dequeue();
				LocalDateTime tiempo = convertirFecha_Hora_LDT(temp.getTicketIssueDate());
				if(temp.getAccidentIndicator().equals("Si"))
				{
				if(hora1.isBefore(tiempo) && hora2.isAfter(tiempo) )
				{
					contadora++;
				}
				
				}
				
			}
			System.out.println("La cantida de accidentes en la hora: "+ hora1 + "es de "+ contadora);
			hora1.plusHours(1);
			hora2.plusHours(1);
				
		}
			return contadora;
	}
	public void countMovingViolationsInHourRangeGraphic(){
		// TODO Auto-generated method stub
		LocalDateTime hora1 = LocalDateTime.parse("00:00:00");
		LocalDateTime hora2 = LocalDateTime.parse("01:00:00");
		int contadora = 0;
		int numeroDeX = 0;
		System.out.println("Porcentaje de infracciones que tuvieron accidentes por hora. 2018");
		System.out.println("Hora "+ " / " + "% de accidentes");
		for(int i = 0; i <24; i++)
		{
			for(int j = 0; j<movingViolationsQueue.size(); j++)
			{
				VOMovingViolations temp = movingViolationsQueue.dequeue();
				LocalDateTime tiempo = convertirFecha_Hora_LDT(temp.getTicketIssueDate());
				if(temp.getAccidentIndicator().equals("Si"))
				{
				if(hora1.isBefore(tiempo) && hora2.isAfter(tiempo) )
				{
					contadora++;
					numeroDeX = (contadora*100)/movingViolationsQueue.size();
				}
				
				}
				
			}
			System.out.println(hora1 + " / ");
			for(int x = 0; x<numeroDeX; x++)
			{
				System.out.print("X");
			}
			hora1.plusHours(1);
			hora2.plusHours(1);
				
		}
			
	}
	public double totalDebt(LocalDate fechaInicial11, LocalDate fechaFinal11) {
		// TODO Auto-generated method stub
		double retorno = 0;
		for(int i = 0; i < movingViolationsStack.size(); i++){
			VOMovingViolations object = movingViolationsStack.pop();
			LocalDateTime fecha =  convertirFecha_Hora_LDT(object.getTicketIssueDate());
			if(fechaInicial11.isBefore(fecha.toLocalDate()) && fechaFinal11.isAfter(fecha.toLocalDate())){
			System.out.println(object.getTotalPaid() + " " + object.getPenalty1() + " " + object.getPenalty2() + " " + object.getFineAmount());
			retorno += object.getFineAmount() - object.getTotalPaid();
			}
			
		}
		return retorno;
	}
	public void deudaAcumuladaPorInfraccionesGraph(){
		System.out.println("Deuda acumulada por mes de infracciones. 2018");
		System.out.println("Mes| Dinero");
		String fecha1 = "01/01/2018";
		String fecha2 = "30/01/2018";
		double total1 = totalDebt(convertirFecha(fecha1), convertirFecha(fecha2));
		String fecha3 = "01/02/2018";
		String fecha4 = "28/02/2018";
		double total2 = totalDebt(convertirFecha(fecha3), convertirFecha(fecha4));
		String fecha5 = "01/03/2018";
		String fecha6 = "30/03/2018";
		double total3 = totalDebt(convertirFecha(fecha5), convertirFecha(fecha6));
		String fecha7 = "01/04/2018";
		String fecha8 = "30/04/2018";
		double total4 = totalDebt(convertirFecha(fecha7), convertirFecha(fecha8));
		
		System.out.println("01| XX");
		System.out.println("02| XXX");
		System.out.println("03 | XXXXXXX");
		System.out.println("04 | XXXXXXXXXXXX");
		
	
		System.out.println("");
		System.out.println("Cada XX representa $YYYY USD");
	}
	
	/**
	 * Convertir fecha a un objeto LocalDate
	 * @param fecha fecha en formato dd/mm/aaaa con dd para dia, mm para mes y aaaa para agno
	 * @return objeto LD con fecha
	 */
	private static LocalDate convertirFecha(String fecha)
	{
		return LocalDate.parse(fecha, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
	}

	
	/**
	 * Convertir fecha y hora a un objeto LocalDateTime
	 * @param fecha fecha en formato dd/mm/aaaaTHH:mm:ss con dd para dia, mm para mes y aaaa para agno, HH para hora, mm para minutos y ss para segundos
	 * @return objeto LDT con fecha y hora integrados
	 */
	 private static LocalDateTime convertirFecha_Hora_LDT(String fechaHora)

     {

                    return LocalDateTime.parse(fechaHora, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'.000Z'"));

     }
}
