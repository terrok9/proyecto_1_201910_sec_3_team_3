package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;
//*  For additional documentation, see <a href="https://algs4.cs.princeton.edu/13stacks">Section 1.3</a> of
//*  <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
//*
//*  @author Robert Sedgewick
//*  @author Kevin Wayne

public class Lista <T> implements IQueue<T> {
	    private int a;         
	    private Nodo primero;    
	    private Nodo ultimo;     

	 
	    private class Nodo {
	        private T elemento;
	        private Nodo siguiente;
	    }

	   
	    public Lista()
	    {
	        primero = null;
	        ultimo  = null;
	        a = 0;
	        
	    }

	    
	    public boolean isEmpty() {
	        return primero == null;
	    }

	    
	    public int size() {
	        return a;     
	    }

	   
	    public T peek() {
	        if (isEmpty()) throw new NoSuchElementException("Queue underflow");
	        return primero.elemento;
	    }

	   
	    public void enqueue(T item) {
	        Nodo ultimoL = ultimo;
	        ultimo = new Nodo();
	        ultimo.elemento= item;
	        ultimo.siguiente= null;
	        if (isEmpty()) primero = ultimo;
	        else           ultimoL.siguiente = primero;
	        a++;
	        
	    }

	 
	    public T dequeue() {
	        if (isEmpty()) throw new NoSuchElementException("Queue underflow");
	        T x = ultimo.elemento;
	        primero = primero.siguiente;
	        a--;
	        if (isEmpty()) ultimo = null;   
	        return x;
	    }

	 
	

	    public Iterator<T> iterator()  {
	        return new ListIterator();  
	    }
	    private class ListIterator implements Iterator<T> {
	        private Nodo actual = primero;

	        public boolean hasNext()  { return actual != null;                     }
	        public void remove()      { throw new UnsupportedOperationException();  }

	        public T next() {
	            if (!hasNext()) throw new NoSuchElementException();
	            T x = actual.elemento;
	            actual = actual.siguiente; 
	            return x;
	        }

	    
	    }
}
