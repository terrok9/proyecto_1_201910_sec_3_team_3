package model.data_structures;

public abstract class Nodo<T extends Comparable<T>>  {
protected T elemento;
	
	protected Nodo<T> siguiente;
	
	public Nodo(T elemento)
	{
		this.elemento = elemento;
	}
	public T darElemento()
	{
		return elemento;
	}
	public void modElemento(T elemento)
	{
		this.elemento = elemento;
	}
	public Nodo<T> darSiguienteNodo()
	{
		return siguiente;
	}
	public void modSiguiente(Nodo<T> siguiente)
	{
		this.siguiente = siguiente;
	}
	
	public String toString()
	{
		return elemento.toString();
	}

}
