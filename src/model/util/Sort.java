package model.util;

import model.data_structures.LinkedList;
import contenedora.VOMovingViolations;

public class Sort {
	/**
 * For additional documentation, see <a href="https://algs4.cs.princeton.edu/21elementary">Section 2.1</a> of
 *  <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
 *  
 *  @author Robert Sedgewick
 *  @author Kevin Wayne
	 */
	/**
	 * Ordenar datos aplicando el algoritmo ShellSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	public static void ordenarShellSort( Comparable<VOMovingViolations>[ ] datos ) {
		
		// TODO implementar el algoritmo ShellSort
		int n = datos.length;

        // 3x+1 increment sequence:  1, 4, 13, 40, 121, 364, 1093, ... 
        int h = 1;
        while (h < n/3) h = 3*h + 1; 

        while (h >= 1) {
            // h-sort the array
            for (int i = h; i < n; i++) {
                for (int j = i; j >= h && less(datos[j], datos[j-h]); j -= h) {
                    exchange(datos, j, j-h);
                }
            }
            h /= 3;
        }
	}
	
	/**
	 * Ordenar datos aplicando el algoritmo MergeSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	public static void ordenarMergeSort( Comparable<VOMovingViolations>[ ] datos ) 
	{

		// TODO implementar el algoritmo MergeSort
		 int n = datos.length;
	        Comparable<VOMovingViolations>[] aux = new Comparable[n];
	        for (int len = 1; len < n; len *= 2) {
	            for (int lo = 0; lo < n-len; lo += len+len) {
	                int mid  = lo+len-1;
	                int hi = Math.min(lo+len+len-1, n-1);
	                merge(datos, aux, lo, mid, hi);
	            }
	        }
	}
	 private static void merge(Comparable<VOMovingViolations>[] datos, Comparable<VOMovingViolations>[] aux, int lo, int mid, int hi) {

	        // copy to aux[]
	        for (int k = lo; k <= hi; k++) {
	            aux[k] = datos[k]; 
	        }

	        // merge back to a[]
	        int i = lo, j = mid+1;
	        for (int k = lo; k <= hi; k++) {
	            if      (i > mid)              datos[k] = aux[j++];  // this copying is unneccessary
	            else if (j > hi)               datos[k] = aux[i++];
	            else if (less(aux[j], aux[i])) datos[k] = aux[j++];
	            else                           datos[k] = aux[i++];
	        }

	    }

	/**
	 * Ordenar datos aplicando el algoritmo QuickSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	public static void ordenarQuickSort( Comparable<VOMovingViolations>[ ] datos ) {

		// TODO implementar el algoritmo QuickSort
	    
		StdRandom.shuffle(datos, 0, datos.length);
        sort(datos, 0, datos.length - 1);
	}
	
	 private static int partition(Comparable<VOMovingViolations>[] a, int lo, int hi) {
	        int i = lo;
	        int j = hi + 1;
	        Comparable<VOMovingViolations> v = a[lo];
	        while (true) { 

	            // find item on lo to swap
	            while (less(a[++i], v)) {
	                if (i == hi) break;
	            }

	            // find item on hi to swap
	            while (less(v, a[--j])) {
	                if (j == lo) break;      // redundant since a[lo] acts as sentinel
	            }

	            // check if pointers cross
	            if (i >= j) break;

	            exchange(a, i, j);
	        }

	        // put partitioning item v at a[j]
	        exchange(a, lo, j);

	        // now, a[lo .. j-1] <= a[j] <= a[j+1 .. hi]
	        return j;
	    }
	  private static void sort(Comparable<VOMovingViolations>[] a, int lo, int hi) { 
	        if (hi <= lo) return;
	        int j = partition(a, lo, hi);
	        sort(a, lo, j-1);
	        sort(a, j+1, hi);
	        
	    }
	
	/**
	 * Comparar 2 objetos usando la comparacion "natural" de su clase
	 * @param v primer objeto de comparacion
	 * @param w segundo objeto de comparacion
	 * @return true si v es menor que w usando el metodo compareTo. false en caso contrario.
	 */
	private static boolean less(Comparable<VOMovingViolations> v, Comparable<VOMovingViolations> w)
	{
		// TODO implementar
		if(v.compareTo((VOMovingViolations) w) == -1){
			return true;
		}
		else{
			return false;
		}
	}
	
	/**
	 * Intercambiar los datos de las posicion i y j
	 * @param datos contenedor de datos
	 * @param i posicion del 1er elemento a intercambiar
	 * @param j posicion del 2o elemento a intercambiar
	 */
	private static void exchange( Comparable<VOMovingViolations>[] datos, int i, int j)
	{
		// TODO implementar
		Object swap = datos[i];
        datos[i] = datos[j];
        datos[j] = (Comparable<VOMovingViolations>) swap;
	}
}
